# This script shows the IP and checks the internet connection
import socket
import os
import sys
import urllib

os.system('clear')

try:
    addr = sys.argv[1]
except:
    addr = ''

try :
    beesite = "http://beetangle.com"
    data = urllib.urlopen(beesite)
    connected = True
except:
    connected = False


if addr and connected:
	print ' '
	print ' '
	print ' '
	print ' '
	print ' '
	print ' '
	print ' '
	print ' '
	print '                            *** Welcome to Beetangle Player ***'
	print ' '
	print '                             Your player is connected to network.'
	print '                        To continue configuring the device, please type'
	print '                          the following address into the address bar' 
	print '                                  of your favourite browser:'
	print '                                 http://' + addr + ':8001'
	print ' '
	print ' '
	print ' '
else:
	print ' '
	print ' '
	print ' '
	print ' '
	print ' '
	print ' '
	print ' '
	print ' '
	print '                            *** Welcome to Beetangle Player ***'
	print ' '
	print '                           It seems your player is not connected'
	print '                          to network and/or internet. Please check'
	print '                            your newtwork connection before you'
	print '                         could be able to finish the configuration.'
	print ' '
	print ' '
	print ' '
