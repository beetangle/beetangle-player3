#!/usr/bin/env python

import os
import shutil
import json
import sys
import signal
import subprocess
import requests
import datetime
import time
from functools import partial
from requests.exceptions import RequestException

from utils import (update_config, build_api_access_headers,
                   build_api_json_headers, get_logger, build_abs_path)

BEETANGLE_ROOT = 'https://app.beetangle.com'
# BEETANGLE_ROOT = 'https://test.beetangle.com'
API_ROOT = BEETANGLE_ROOT + '/api/v1/'

logger = get_logger('downloader')

function_set_error_if_canceled = None


class ApiQueueStatus(object):
    IN_PROGRESS = 'publish in process'
    PUBLISHED = 'published'
    ERROR = 'error'


def run_downloader():
    global function_set_error_if_canceled

    update_config({
        'last_downloader_run': datetime.datetime.utcnow().isoformat(),
    })

    access_token = read_token()
    if not access_token:
        logger.warning('No API access token.')
        return

    channel_id = read_channel_id()

    queue = get_queue_to_be_published(access_token, channel_id)

    function_set_error_if_canceled = partial(update_queue_status_for_channel,
                                             access_token, queue['id'],
                                             channel_id, ApiQueueStatus.ERROR)
    cancel_others()

    update_queue_status_for_channel(access_token, queue['id'], channel_id,
                                    ApiQueueStatus.IN_PROGRESS)

    try:
        playlist = download_playlist(queue['stories_media'])
        restart_player_with_playlist(playlist)
    except Exception, e:
        update_queue_status_for_channel(access_token, queue['id'], channel_id,
                                        ApiQueueStatus.ERROR, message=str(e))
        raise

    update_queue_status_for_channel(access_token, queue['id'], channel_id,
                                    ApiQueueStatus.PUBLISHED)
    update_config({
        'queue_name': queue['name'],
    })


def get_queue_to_be_published(access_token, channel_id):
    params = {'ready_for_channel': channel_id, 'expand': 'stories_media'}
    r = requests.get(API_ROOT + 'queues/', params=params,
                     headers=build_api_access_headers(access_token),
                     verify=False)

    if r.status_code != 200:
        logger.warning('Getting of queue to be published failed (channel=%s, '
                       'response_code=%s, response=%s).',
                       channel_id, r.status_code, r.text)
        sys.exit(1)

    response = r.json()
    if len(response['objects']) == 0:
        logger.info('No queues to be published (channel=%s).', channel_id)
        sys.exit()

    queue = response['objects'][0]
    if len(queue['stories_media']) == 0:
        logger.warning('Queue to be published is empty (id=%s).', queue['id'])

    return queue


def get_clips_path():
    return build_abs_path('../player/clips/')


def download_playlist(media_list):
    playlist = []
    clips_path = get_clips_path()
    for media in media_list:
        #download media files to temp folder + building playlist
        logger.info('Downloading ... %s', media['hires_url'])

        media_uri = BEETANGLE_ROOT + media['hires_url']
        media_file_name = os.path.basename(media['hires_url'])
        media_file_path = os.path.join(clips_path, media_file_name)

        playlist.append({
            'clip_name': media['name'],
            'clip_file': media_file_name,
            'clip_thumbnail': BEETANGLE_ROOT + media['thumbnail_url']
        })

        if not(os.path.isfile(media_file_path)):
            r = requests.get(media_uri, stream=True, verify=False)
            tmp_media_file_path = media_file_path + '.download'
            with open(tmp_media_file_path, 'wb') as f:
                for chunk in r.iter_content(chunk_size=1024):
                    f.write(chunk)
            os.rename(tmp_media_file_path, media_file_path)
            logger.info('Done ...')
        else:
            logger.info('Already exists, don\'t download')

    return playlist


def restart_player_with_playlist(playlist):
    clips_path = get_clips_path()

    #save playlist
    with open(os.path.join(clips_path, 'playlist.json'), 'w') as f:
        json.dump(playlist, f)

    #Stop Player
    p = subprocess.Popen(build_abs_path('../bin/qplay'), shell=True)
    out, err = p.communicate()

    #delete files from clips folder that are not in current playlist
    clip_files = [i['clip_file'] for i in playlist]
    for f in os.listdir(clips_path):
        f_path = os.path.join(clips_path, f)
        if f in clip_files or not os.path.isfile(f_path):
            continue
        if f not in ('playlist.json', '.gitignore'):
            os.remove(f_path)

    #delete content of temp folder, kind of migration for current devices
    #TODO: this can be removed in the future
    temp_path = build_abs_path('temp/')
    for f in os.listdir(temp_path):
        f_path = os.path.join(temp_path, f)
        if os.path.isfile(f_path) and f != '.gitignore':
            os.remove(f_path)

    #Runs player again
    if playlist:
        p = subprocess.Popen('python ' + build_abs_path('../player/player.py'),
                         shell=True)
        out, err = p.communicate()


def cancel_others():
    p = subprocess.Popen(['ps', '-o', 'pid comm args'], stdout=subprocess.PIPE)
    out, err = p.communicate()
    for line in out.splitlines():
        pid, command, args = line.split(None, 2)
        if 'python' in command and 'downloader.py' in args:
            if str(os.getpid()) == pid:
                continue
            logger.warning('Going to cancel another downloader (pid=%s)', pid)
            os.kill(int(pid), signal.SIGINT)


def update_queue_status_for_channel(access_token, queue_id, channel_id, status,
                                    message=None):
    data = {'status': status, 'channels': [channel_id, ]}
    if message:
        data['message'] = message

    try:
        r = requests.post(API_ROOT + 'queues/%s/publish/confirm/' % queue_id,
                          data=json.dumps(data),
                          headers=build_api_json_headers(access_token),
                          verify=False)
    except RequestException, e:
        logger.exception(e)     # just log it, do not interrupt publishing
        return

    if r.status_code != 200:
        logger.warning('Failed to update queue status (queue=%s, channel=%s, '
                       'status=%s, response_code=%s, response=%s).',
                       queue_id, channel_id, status, r.status_code, r.text)


def read_token():
    try:
        with open(build_abs_path('../config/token'), 'r') as f:
            return f.read()
    except IOError:
        return None


def read_channel_id():
    with open(build_abs_path('../config/chid'), 'r') as f:
        return f.read()


def sigint_handler(signum, frame):
    global function_set_error_if_canceled

    logger.warning('Process cancelled by user or another downloader.')
    if function_set_error_if_canceled:
        function_set_error_if_canceled(message='Canceled by publishing another '
                                               'queue')
    sys.exit()


if __name__ == '__main__':
    signal.signal(signal.SIGINT, sigint_handler)
    try:
        run_downloader()
    except Exception, e:
        logger.exception(e)
        sys.exit('Terminated by exception.')
