#!/bin/bash
set -e

cd /home/pi/beetangle-player/downloader
# activating the virtualenv
# TODO: make more generic path
source /home/pi/.virtualenvs/player/bin/activate

# run downloader
exec python downloader.py
