#!/usr/bin/env python

import sys
import subprocess
import json
import time

from utils import get_logger, build_abs_path, read_config

PLAYLIST_PATH = build_abs_path('clips/playlist.json')
PLAYER_PATH = build_abs_path('../bin/play')

logger = get_logger('player')


def read_playlist(playlist_path):
    try:
        with open(playlist_path) as f:
            return json.load(f)
    except ValueError:
        return []


def get_clip_paths(playlist):
    clips_dir = build_abs_path('clips/')
    return [clips_dir + clip['clip_file'] for clip in playlist]


def start_player(player_path, playlist):
    clip_paths = get_clip_paths(playlist)
    config = read_config()
    cmd = [player_path, '-o', config.get('audio', 'local')] + clip_paths
    p = subprocess.Popen(build_abs_path('../bin/qplay'), shell=True)
    out, err = p.communicate()
    time.sleep(1)
    p = subprocess.Popen(' '.join(cmd), shell=True)
    time.sleep(1)

if __name__ == '__main__':
    try:
        playlist = read_playlist(PLAYLIST_PATH)
        if playlist:
            start_player(PLAYER_PATH, playlist)
    except IOError:
        logger.info('Nothing to play, stopping.')
    except Exception, e:
        logger.exception(e)
        sys.exit('Terminated by exception.')
