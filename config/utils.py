import os
import json
import logging
import logging.config

logging_initialized = False


def build_api_access_headers(access_token):
    return {
        'Authorization': 'Bearer ' + access_token,
    }


def build_api_json_headers(access_token):
    return {
        'Authorization': 'Bearer ' + access_token,
        'Content-Type': 'application/json',
    }


def read_config():
    try:
        with open(build_abs_path('../config/config.json'), 'r') as f:
            return json.load(f)
    except IOError:
        return {}


def write_config(config):
    with open(build_abs_path('../config/config.json'), 'w') as f:
        json.dump(config, f)


def update_config(update_by_dict):
    config = read_config()
    config.update(update_by_dict)
    write_config(config)


def build_abs_path(path):
    return os.path.join(os.path.dirname(__file__), path)


def init_logging():
    global logging_initialized

    if logging_initialized:
        return

    LOGGING = {
        'version': 1,
        'formatters': {
            'simple': {
                'format': "%(asctime)s - %(name)s - %(levelname)s - %(message)s",
            }
        },

        'handlers': {
            'console': {
                'level': 'DEBUG',
                'class': 'logging.StreamHandler',
                'formatter': 'simple'
            },
            'file': {
                'level': 'DEBUG',
                'class': 'logging.handlers.RotatingFileHandler',
                'filename': build_abs_path('../logs/beetangle.log'),
                'maxBytes': 10485760,
                'backupCount': 5,
                'formatter': 'simple'
            },
        },

        'root': {
            'handlers': ['console', 'file'],
            'level': 'DEBUG',
        },
    }

    logging.config.dictConfig(LOGGING)
    logging_initialized = True


def get_logger(name):
    init_logging()
    return logging.getLogger(name)
